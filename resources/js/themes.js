import { ref } from "vue"

localStorage.setItem('themes', localStorage.getItem('themes') || JSON.stringify({
    topbar: 'bg-white/95 border-b border-slate-200 text-gray-700 hover:bg-white hover:text-gray-800',
    sidebar: 'bg-white text-slate-400 hover:bg-slate-200 hover:text-gray-500',
}))

const themes = ref(JSON.parse(localStorage.getItem('themes')))

const set = (key, val) => {
    themes.value[key] = val
    localStorage.setItem('themes', JSON.stringify(themes.value))
}

const get = (key, def) => themes.value[key] || def

export default {
    ...themes.value,
    set,
    get
}
