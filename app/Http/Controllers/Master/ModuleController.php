<?php

namespace App\Http\Controllers\Master;

use Inertia\Inertia;
use App\Models\Module;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ModuleController extends Controller
{
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */


  public function index(Request $request)
  {
    return Inertia::render('Master/Module/Index', [
        'datum' => Module::query()
            ->when($request->input('search'), function ($query, $search){
                $query->where('name', 'like', "%{$search}%");
            })
            ->paginate(10)
            ->withQueryString()
            ->through(fn($data) => [
                'id' => $data->id,
                'name' => $data->name,
                'table_name' => $data->table_name,
                'slug' => $data->slug,
                'description' => $data->description,
                'status' => $data->status,
                'isShow' => 0,
            ]),
        'filters' => $request->only(['search'])
    ]);
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create()
  {
    $table_name = DB::select('SHOW TABLES like "sys_%"');
    $table_name = array_map('current',$table_name);
    // dd($table_name);

    return Inertia::render('Master/Module/Create', [
        'table_name' => $table_name
    ]);
  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(Request $request)
  {
    $user_id = Auth::user()->id;

    $this->validate($request,[
        'name' => ['required'],
        'table_name' => ['required']
    ]);

    $data = [];
    $data['name'] = $request->name;
    $data['table_name'] = $request->table_name;
    $data['description'] = $request->description;
    $data['slug'] = Str::slug($request->name);
    $data['status'] = 1;
    $data['user_id'] = $user_id;
    $data['created_at'] = now();
    $data['updated_at'] = now();

    Module::insert($data);

    return redirect()->route('master.module.index');
  }

  /**
  * Display the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function show(Module $module)
  {

    $table_name = DB::select('SHOW TABLES');
    $table_name = array_map('current',$table_name);

    return Inertia::render('Master/Module/View',[
        'datum' => $module,
        'table_name' => $table_name
    ]);
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function edit(Module $module)
  {

    $table_name = DB::select('SHOW TABLES');
    $table_name = array_map('current',$table_name);

    return Inertia::render('Master/Module/Edit',[
        'datum' => $module,
        'table_name' => $table_name
    ]);
  }

  public function toggle($id, $fstatus)
  {

    $fstatus = ($fstatus === 'true' ? 1 : 0);
        $data = [];
        $data['status'] = $fstatus;
        Module::where('id','=', $id)->update($data);

        return redirect()->route('master.module.index');
  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request, Module $module)
  {

    $user_id = Auth::user()->id;
    $this->validate($request,[
        'name' => ['required'],
        'table_name' => ['required']
    ]);

    $module->update([
        'name' => $request->name,
        'table_name' => $request->table_name,
        'description' => $request->description,
        'slug' => Str::slug($request->name),
        'user_id' => $user_id,
        'updated_at' => now()
    ]);

    return redirect()->route('master.module.index');
  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function destroy(Module $module)
  {

    $module->delete();

    return redirect()->route('master.module.index');
  }
}
