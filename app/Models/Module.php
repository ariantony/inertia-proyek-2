<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Module extends Model
{
  use HasFactory;
  use SoftDeletes;

  protected $table = 'sys_tmst_module';

  protected $fillable = [
    'id',
    'name',
    'table_name',
    'slug',
    'description',
    'status',
    'user_id',
    'created_at',
    'updated_at',
    'deleted_at'
  ];

  protected $dates = ['deleted_at'];
}
