<?php

namespace Database\Seeders;
use Faker\Factory as Faker;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class ModuleSeeder extends Seeder
{
  /**
  * Run the database seeds.
  *
  * @return void
  */
  public function run()
  {
    $faker = Faker::create('id ID');
    for($i=1; $i < 52; $i++) {
        DB::table('sys_tmst_module')->insert([
            'name' => $faker->name,
            'table_name' => $faker->name,
            'slug' => Str::slug($faker->name),
            'description' => $faker->text(),
            'status' => '1',
            'user_id' => '1',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }
  }
}
