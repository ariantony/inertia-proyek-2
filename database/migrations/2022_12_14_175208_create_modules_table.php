<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
  /**
  * Run the migrations.
  *
  * @return void
  */
  public function up()
  {
    Schema::create('sys_tmst_module', function (Blueprint $table) {
      $table->id();
      $table->string('name');
      $table->string('table_name');
      $table->string('slug')->nullable();
      $table->string('description')->nullable();
      $table->boolean('status')->default('1');
      $table->id('user_id')->autoIncrement(false);
      $table->timestamps();
      $table->softDeletes();
    });
  }
  /**
  * Reverse the migrations.
  *
  * @return void
  */
  public function down()
  {
    Schema::dropIfExists('sys_tmst_module');
  }
};
